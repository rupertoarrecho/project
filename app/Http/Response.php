<?php

namespace App\Http;

class Response
{
    protected $response; //array, json, pdf...

    public function __construct($response)
    {
        $this->response = $response; //home, contact
    }

    public function getResponse()
    {

        return $this->response;
    }

    public function send()
    {
        $response = $this->getResponse();

        $content = $response;

        //require __DIR__ . "/../../views/layout.php";
        require viewPath('show'); //Better this was using helpers. Repetitive Code removed
    }
}