<?php

namespace App\Http\Controllers;
//use App\Http\Response; Not necessary if used helper funciton of view()

class SkillsController
{
    public function index()
    {
        return resp(['Python', 'PHP', 'Javascript', 'Scripts', 'Kubernetes', 'Docker', 'Google Cloud', 'CI/CD pipelines']);
    }
}