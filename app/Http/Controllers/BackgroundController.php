<?php

namespace App\Http\Controllers;
//use App\Http\Response; Not necessary if used helper funciton of view()

class BackgroundController
{
    public function index()
    {
        return resp(['5th Generation Mobile Networks Module', 'Courses with accredited certificates of Cloud Networks topics', 'Work Experience in Cloud Architecture with Deployment of Kubernetes Services in Google Cloud at Endava company.']);
    }
}