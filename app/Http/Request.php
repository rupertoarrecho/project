<?php

namespace App\Http;

class Request
{
    protected $segments = [];
    protected $controller;

    public function __construct()
    {
        // platzi.test/servicios/index
        $this->segments = explode('/', $_SERVER['REQUEST_URI']);

        $this->setController();
    }

    public function setController()
    {
        $this->controller = empty($this->segments[1])
            ? 'home'
            : $this->segments[1];

    }


    public function getController()
    {
        //home, Home
        $controller = strtolower($this->controller);

        $controller = ucfirst($controller);

        return "App\Http\Controllers\\{$controller}Controller";
    }

    public function send()
    {
        $controller = $this->getController();

        $response = call_user_func([
            new $controller,
            'index'
        ]);

        $response->send();
    }
}