<?php

if(! function_exists('resp')) {
    
    function resp($resp) {
        return new App\Http\Response($resp);
    }
}

if(! function_exists('viewPath')) {
    function viewPath($view) {
        return __DIR__ . "/../views/$view.php";
    }
}